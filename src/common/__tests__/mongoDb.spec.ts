/* eslint-disable no-console */
import { MongoMemoryServer } from 'mongodb-memory-server';
import { config } from '@dk/module-config';
import {
  connectMongo,
  MONGO_DB_NAME,
  MONGO_PASS_PARAM,
  MONGO_USER_PARAM,
  MONGO_HOSTS,
  MONGO_RECONNECT_INTERVAL,
  MONGO_RECONNECT_RETRIES
} from '../mongoDb';
import mongoose from 'mongoose';

jest.mock('@dk/module-config');
jest.mock('../../logger', () => ({
  info: console.info,
  error: console.error
}));

describe('dbConnect', () => {
  const mockConfig = (
    uri: string,
    dbName?: string,
    username?: string,
    pwd?: string,
    interval?: number,
    retries?: number
  ) => {
    (config.get as jest.Mock).mockImplementation(key => {
      switch (key) {
        case MONGO_USER_PARAM: {
          return username;
        }
        case MONGO_DB_NAME: {
          return dbName;
        }
        case MONGO_HOSTS: {
          return [uri];
        }
        case MONGO_PASS_PARAM: {
          return pwd;
        }
        case MONGO_RECONNECT_RETRIES: {
          return retries;
        }
        case MONGO_RECONNECT_INTERVAL: {
          return interval;
        }
        default: {
          return undefined;
        }
      }
    });
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('connectMongo', () => {
    let mongod: MongoMemoryServer;
    const dbName = 'myDB';

    beforeEach(() => {
      mongod = new MongoMemoryServer({
        instance: {
          dbName
        }
      });
    });

    afterEach(async () => {
      await mongoose.disconnect();
      await mongod.stop();
    });

    it(`should reject if ${MONGO_DB_NAME} is not provided`, async () => {
      mockConfig('mongodb://localhost');
      const error = await connectMongo().catch(err => err);
      expect(error).toBeDefined();
    });

    it('should reject if on first connection fail if retry not set', async () => {
      jest.spyOn(mongoose, 'connect').mockImplementationOnce((): any => {
        mongoose.connection.emit('error', new Error());
        return new Promise(resolve => {
          resolve();
        });
      });
      const port = await mongod.getPort();
      mockConfig(`127.0.0.1:${port}`, 'dbName', '', '');
      const error = await connectMongo().catch(err => err);

      expect(error).toBeDefined();
    });

    it('should reconnect on connection fail', async () => {
      const port = await mongod.getPort();
      mockConfig(`127.0.0.1:${port}`, dbName, '', '', 100);
      await connectMongo();

      const isReconnected = await new Promise(resolve => {
        mongoose.connection.on('reconnected', () => {
          resolve(true);
        });
        mongod.stop().then(() => {
          mongod = new MongoMemoryServer({
            instance: {
              dbName,
              port
            }
          });
        });
      });
      expect(isReconnected).toEqual(true);
    });
  });
});

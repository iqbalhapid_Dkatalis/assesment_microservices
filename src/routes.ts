import healthcheck from './healthcheck/healthcheck.controller';
import userController from './user/user.controller';

const routes = [...healthcheck, ...userController];

export { routes };
